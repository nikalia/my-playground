import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import LoginPage from './Login';

class LandingPage extends React.Component {
  constructor(props){
    super(props);
  }
  logoutClick() {
    this.props.changeComponent('LoginPage');
  }
  render() {
    return(
      <div className='App-header'>
      <div className='App'>
      <h3> Welcome </h3> <br/>
      <button onClick={this.logoutClick.bind(this)}>
      Logout
      </button>
      </div>
      </div>
    );
  }
}

export default LandingPage;
