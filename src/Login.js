import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    name: '',
    password:'',
    myUsername:'nik',
    myPassword: '1',
    componentSelected: 'LoginPage'
  }
  }
  nameText(e) {
    this.setState({name: e.target.value});
  }
  passwordText(e) {
    this.setState({password: e.target.value});
  }
  loginClick(e) {
    e.preventDefault();
    if (this.state.name !== this.state.myUsername || this.state.password !== this.state.myPassword) {
      alert('Please re-enter');
    } else {
      //alert('Sukshess');
      return this.props.changeComponent('LandingPage');
    }
  }
  Reset() {
    this.setState({
      name: "",
      password: ""
  });
}
  render() {
    return (
      <div className='App'>
      <div className='App-header'>
      <form onSubmit={this.loginClick.bind(this)} onReset={this.Reset.bind(this)}>
        <label>
          Name:
          <input type="text" onChange={this.nameText.bind(this)}/>
          <br/>
          <br/>
          Password:
          <input type="text"  onChange={this.passwordText.bind(this)}/>
          <br/>
        <br/>
        <input type="submit" value="Login" />
        <input type="reset" value="Reset" />
        </label>
      </form>
      </div>
      </div>
    );
  }
}

export default LoginPage;
