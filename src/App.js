import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import LoginPage from './Login';
import LandingPage from './Land';

class App extends React.Component {
  constructor() {
    super();
      this.changeComponent = this.changeComponent.bind(this);
      this.state = { componentSelected: 'LoginPage'}
}
  changeComponent(component) {
    this.setState({componentSelected: component});
}
  renderComponent(component) {
    if(component === 'LoginPage') {
      return <LoginPage changeComponent = {this.changeComponent} />;
    } else if (component === 'LandingPage') {
      return <LandingPage changeComponent = {this.changeComponent} />;
    }
  }
  render() {
    return (
      <div>
      {this.renderComponent(this.state.componentSelected)}
      </div>
    );
  }
}

export default App;
